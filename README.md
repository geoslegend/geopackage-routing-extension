GeoPackage Routing Extension
===============

Networking and routing capability as a GeoPackage extension


[![build status](https://gitlab.com/GitLabRGI/geopackage-routing-extension/badges/master/build.svg)](https://gitlab.com/GitLabRGI/geopackage-routing-extension/commits/master)
[![coverage report](https://gitlab.com/GitLabRGI/geopackage-routing-extension/badges/master/coverage.svg)](https://gitlab.com/GitLabRGI/geopackage-routing-extension/commits/master)

Under construction...
=======
## Network and routing packages for GeoPackage routing extension

